#! /usr/bin/env python3

import math
import pathlib
import rdflib

datasetPath = "phenotypeToNumberOfDiseases.ttl"
ICdatasetPath = "phenotypeToInformationContentNumberOfDiseases.ttl"

rdflibFormat = {}
rdflibFormat['.owl'] = 'xml'
rdflibFormat['.ttl'] = 'ttl'
datasetFormat = rdflibFormat[pathlib.PurePath(datasetPath).suffix]

rdfGraph = rdflib.Graph()
rdfGraph.load(datasetPath, format=datasetFormat)

sparqlQuery = pathlib.Path('./queries/getHighestNumberOfDiseases.rq').read_text()
qres = rdfGraph.query(sparqlQuery)
highestNumberOfDiseases = int(qres.bindings[0]['maxNbDiseases'])


sparqlQuery = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX dct: <http://purl.org/dc/terms/>

PREFIX HP: <http://purl.obolibrary.org/obo/HP_>
PREFIX hpoa: <http://hpo.jax.org/annotation/>

SELECT ?pheno ?nbDiseases
WHERE {
  ?pheno hpoa:numberOfDiseases ?nbDiseases .
}
"""
qres = rdfGraph.query(sparqlQuery)
with open(ICdatasetPath, "w") as ICdatasetFile:
	ICdatasetFile.write("""
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix dct: <http://purl.org/dc/terms/> .

@prefix HP: <http://purl.obolibrary.org/obo/HP_> .
@prefix hpoa: <http://hpo.jax.org/annotation/> .

""")
	qres = rdfGraph.query(sparqlQuery)
	for row in qres:
		ICdatasetFile.write("{} hpoa:ICnumberOfDiseases \"{}\"^^xsd:float .\n".format(row[0].replace("http://purl.obolibrary.org/obo/HP_", "HP:"), - math.log(int(row[1])/highestNumberOfDiseases)))
