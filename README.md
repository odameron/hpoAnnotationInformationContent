# hpoAnnotationInformationContent

Compute information content values for Human Phenotype Ontology (https://hpo.jax.org) terms and store them in a graph.